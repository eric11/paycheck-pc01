<?php

include_once 'vendor/autoload.php';

use PayBreak\PayCheck\RpcServer;

$server = new RpcServer();
$server->executeCall(RpcServer::class);
