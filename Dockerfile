FROM ubuntu:17.04
USER root
ENV HOME /root
ENV DEBIAN_FRONTEND noninteractive
ENV TERM xterm

# Packages
RUN apt-get update; \
    apt-get -y install \
        software-properties-common \
        python-software-properties \
        language-pack-en-base \
        zip \
        unzip \
        curl \
        wget \
        nano

# Apache 2.4
RUN apt-get update; \
    apt-get -y install \
        apache2; \
    rm -rf /var/log/apache2/*; \
    rm -rf /var/log/php/*; \
    rm -rf /var/www/*; \
    # apache config
    a2dissite 000-default; \
    a2enmod rewrite; \
    a2enmod headers; \
    a2enmod status

# Set PHP environment
ENV PHP_APACHE_INI             /etc/php/7.1/apache2/php.ini
ENV PHP_CLI_INI                /etc/php/7.1/cli/php.ini  
ENV PHP_APACHE_APP_INI         /etc/php/7.0/apache2/conf.d/10-app.ini
ENV PHP_CLI_APP_INI            /etc/php/7.0/cli/conf.d/10-app.ini
ENV PHP_MODS_AVAILABLE_DIR     /etc/php/7.1/mods-available
ENV PHP_ERROR_LOG              ${LOG_DIR}/php.log
ENV XDEBUG_LOG                 ${LOG_DIR}/xdebug.log

# Php log directories
RUN touch ${PHP_ERROR_LOG}; \
    chown ${APACHE_USER}:${APACHE_GROUP} ${PHP_ERROR_LOG}; \
    chmod 600 ${PHP_ERROR_LOG}; \
    touch ${XDEBUG_LOG}; \
    chown ${APACHE_USER}:${APACHE_GROUP} ${XDEBUG_LOG}; \
    chmod 600 ${XDEBUG_LOG}

# Install PHP 7.1
RUN LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php; \
    apt-get update; \
    apt-get -y install \
        php7.1 \
        php7.1-cli \
        php7.1-mysql \
        php7.1-sqlite3 \
        php7.1-mcrypt \
        php7.1-dom \
        php7.1-mbstring \
        php7.1-zip \
        php7.1-xml \
        php7.1-intl

# Admin user
ENV ADMIN_GROUP admin
ENV ADMIN_USER  admin
ENV ADMIN_GID   50000
ENV ADMIN_UID   50000
ENV ADMIN_HOME  /home/${ADMIN_USER}
RUN \
    groupadd -g ${ADMIN_GID} ${ADMIN_GROUP}; \
    useradd -d ${ADMIN_HOME} -s /bin/bash -g ${ADMIN_GROUP} -u ${ADMIN_UID} ${ADMIN_USER}; \
    mkdir ${ADMIN_HOME}; \
    chown ${ADMIN_USER}:${ADMIN_GROUP} ${ADMIN_HOME}; \
    chmod 700 ${ADMIN_HOME}

# Install Composer
ENV COMPOSER_HOME ${ADMIN_HOME}/.composer
RUN mkdir ${COMPOSER_HOME}; \
    chown ${ADMIN_USER}:${ADMIN_GROUP} ${COMPOSER_HOME}; \
    chmod 770 ${COMPOSER_HOME}; \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
ENV PATH ${COMPOSER_HOME}/vendor/bin:$PATH

# Make /app directory
ENV APP_DIR /app
RUN mkdir ${APP_DIR}; \
    chown ${ADMIN_USER}:${ADMIN_GROUP} ${APP_DIR}; \
    chmod 775 ${APP_DIR}

# Build the application
ENV HOME ${ADMIN_HOME}

# Copy source files
COPY ./ ${APP_DIR}/

# Set app permissions
RUN chown -R ${ADMIN_USER}:${ADMIN_GROUP} ${APP_DIR}
    
# Server config files
USER root
COPY apache/apache2.conf          /etc/apache2/
COPY apache/app.conf              /etc/apache2/sites-available/
COPY apache/ports.conf            /etc/apache2/
COPY php/10-app.ini               ${PHP_APACHE_APP_INI}
COPY php/10-app.ini               ${PHP_CLI_APP_INI}
COPY supervisord/supervisord.conf /etc/supervisor/conf.d/

# Build app
USER ${ADMIN_USER}
RUN cd ${APP_DIR}; \
    composer dumpautoload; \
    composer update --optimize-autoloader

# Configure the executable
LABEL name="PayBreak/PayCheck" description="Paycheck Programming Challenge 01"
EXPOSE 80
EXPOSE 443
WORKDIR ${APP_DIR}
USER root
ENTRYPOINT ["/usr/sbin/apachectl"]
CMD ["-DFOREGROUND"]
