<?php
namespace PayBreak\PayCheck;

use PayBreak\PayCheck\Rpc\Mapper\Gcd as GcdMapper;
use PayBreak\PayCheck\Rpc\Mapper\Flattener as FlattenerMapper;
use PayBreak\Rpc\Request;
use PayBreak\Stdlib\Date;
use function PayBreak\Stdlib\flatten;
use function PayBreak\Stdlib\gcd;

class RpcServer
{
    use \PayBreak\Rpc\Api;

    protected function getActions()
    {
        return [
            'date'      => [self::class, 'date'],
            'flattener' => [self::class, 'flattener'],
            'gcd'       => [self::class, 'gcd'],
        ];
    }
    
    protected function date()
    {
        $date = new Date();
        return [
            'timestamp'     => $date->getTimestamp(), 
            'today'         => $date->format(Date::DDMMYYYY),
            'current_month' => $date->format(Date::MONTH),
            'present'       => $date->format(Date::RFC2822)
        ];
    }

    protected function flattener(array $params)
    {
        return ['result' => flatten($params['object'])];
    }

    protected function gcd(array $params)
    {
        return ['gcd' => gcd($params['a'], $params['b'])];
    }

    protected function authenticate()
    {
        return true;
    }

    protected function getRequestAction()
    {
        return Request::getParam('action');
    }

    protected function getRequestParams()
    {
        switch (Request::getParam('action')) {
            case 'flattener':
                return FlattenerMapper::fromRequest(); 
            case 'gcd':
                return GcdMapper::fromRequest();;
            default:
                return [];
        }
    }
}
