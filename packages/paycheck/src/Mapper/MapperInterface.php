<?php
namespace PayBreak\PayCheck\Rpc\Mapper;

use PayBreak\Rpc\Request;

interface MapperInterface
{
    public static function fromRequest(): array;
}
