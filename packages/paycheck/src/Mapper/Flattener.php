<?php
namespace PayBreak\PayCheck\Rpc\Mapper;

use PayBreak\Rpc\Request;

class Flattener implements MapperInterface
{
    public static function fromRequest(): array
    {
        return ['object' => (array) json_decode(Request::getParam('object'))];
    }
}
