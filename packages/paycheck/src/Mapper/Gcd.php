<?php
namespace PayBreak\PayCheck\Rpc\Mapper;

use PayBreak\Rpc\Request;

class Gcd implements MapperInterface
{
    public static function fromRequest(): array
    {
        return ['a' => Request::getParam('a'), 'b' => Request::getParam('b')];
    }
}
