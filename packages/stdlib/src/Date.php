<?php
namespace PayBreak\Stdlib;

/**
 * Date with custom formats.
 */
class Date extends \DateTime
{
    const DDMMYYYY = 'd/m/Y';
    const MONTH    = 'F';
}
