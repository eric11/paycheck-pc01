<?php
namespace PayBreak\Stdlib;

/**
 * Recursively reduces multidimensional arrays to a one-dimensional array.
 */
function flatten(array $array, string $glue = '-', string $glued = '', array $flattened = [])
{
    foreach($array as $key => $value) {
        if(is_array($value) || is_object($value)) {
            $flattened = flatten((array) $value, $glue, $glued.$key.$glue, $flattened);
        } else {
            $flattened[$glued.$key] = $value;
        }
    }
    return $flattened;
}
