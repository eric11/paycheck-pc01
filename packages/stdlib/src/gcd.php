<?php
namespace PayBreak\Stdlib;

/**
 * Calculate the Greatest Common Divisor of a and b.
 */
function gcd(int $a, int $b): int
{
    while ($b) {
        $b = $a % ($gcd = $b);
        $a = $gcd;
    }
    return $gcd ?? $a;
}
