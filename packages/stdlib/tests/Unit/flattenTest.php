<?php
namespace Tests\Unit;

use function PayBreak\Stdlib\flatten;

class flattenTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     * @dataProvider oneDimensionalArrayProvider
     */
    public function it_flattens_array_to_one_dimensional_array(array $array, array $expected)
    {
        $this->assertEquals($expected, flatten($array));
    }

    /**
     * @test
     * @dataProvider multidimensionalArrayProvider
     */
    public function it_flattens_multidimensional_to_one_dimensional_array(array $array, array $expected)
    {
        $this->assertEquals($expected, flatten($array));
    }
 
    public function oneDimensionalArrayProvider() {
        $array = ['a' => 12, 'c' => 15, 'd' => 20];
        $expected = ['a' => 12, 'c' => 15, 'd' => 20];
        return [
            [$array, $expected],
        ];
    }
 
    public function multidimensionalArrayProvider() {
        $array = ['a' => ['b' => [100 => 'Lorem Ipsum', 'x-y_z' => 0.5], 'c' => 15], 'd' => 20];
        $expected = ['a-b-100' => 'Lorem Ipsum', 'a-b-x-y_z' => 0.5, 'a-c' => 15, 'd' => 20];
        return [
            [$array, $expected],
        ];
    }
}
