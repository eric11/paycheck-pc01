<?php
namespace Tests\Unit;

use function gmp_gcd;
use function gmp_strval;
use function PayBreak\Stdlib\gcd;

class gcdTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     * @dataProvider commonDivisibleNumbersProvider
     */
    public function it_calculates_the_greatest_common_divisor($a, $b)
    {
        $this->assertEquals(gmp_strval(gmp_gcd($a, $b)), gcd($a, $b));
    }

    /**
     * @test
     * @dataProvider zeroNumbersProvider
     */
    public function it_handles_zero($a, $b)
    {
        $this->assertEquals(gmp_strval(gmp_gcd($a, $b)), gcd($a, $b));
    }

    /**
     * @test
     * @dataProvider sameNumbersProvider
     */
    public function it_handles_same_numbers($a, $b)
    {
        $this->assertEquals(gmp_strval(gmp_gcd($a, $b)), gcd($a, $b));
    }
 
    public function commonDivisibleNumbersProvider() {
        return [
            [2, 4],
            [3, 9],
            [60, 90],
            [90, 60],
            [10, 5],
            [3, 1],
        ];
    }
 
    public function zeroNumbersProvider() {
        return [
            [0, 0],
            [1, 0],
            [0, 1],
        ];
    }
 
    public function sameNumbersProvider() {
        return [
            [1, 1],
            [11, 11],
        ];
    }
}
