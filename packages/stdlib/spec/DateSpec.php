<?php

namespace spec\PayBreak\Stdlib;

use PayBreak\Stdlib\Date;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class DateSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Date::class);
    }

    function it_formats_to_dd_mm_yy()
    {
        $this->format(Date::DDMMYYYY)->shouldBeEqualTo((new Date)->format('d/m/Y'));
    }

    function it_formats_to_full_textual_representation_of_month()
    {
        $this->format(Date::MONTH)->shouldBeEqualTo((new Date)->format('F'));
    }

    function it_formats_to_rfc_2822()
    {
        $this->format(Date::RFC2822)->shouldBeEqualTo((new Date)->format(Date::RFC2822));
    }
}
