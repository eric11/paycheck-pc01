PayBreak: Programming Challenge 01
==================================


Docker Usage
------------

Download Docker then run:

.. code-block:: bash

    # Docker-Compose
    $ sudo docker-compose up -d

    # Attach to bash of container
    $ docker exec -it paycheck bash

This will start the RPC server inside the container. You can now browse to: 

Usage
-----

*Composer install:*

.. code-block:: bash

    $ composer install

*Start RPC server:*

.. code-block:: bash

    $ composer start

*Calls*

.. code-block:: bash

    # Date
    $ GET: http://localhost:3000?action=date
    # {"timestamp":1509363032,"today":"30\/10\/2017","current_month":"October","present":"Mon, 30 Oct 2017 11:30:32 +0000"}

.. code-block:: bash

    # Flattener
    $ GET: http://localhost:3000/?action=flattener&object={"a": [{"b": 12}, {"c": 15}], "d":"20"}
    # {"result":{"a-0-b":12,"a-1-c":15,"d":"20"}}

.. code-block:: bash

    # Greatest Common Divisor (GCD)
    $ GET: http://localhost:3000/?action=gcd&a=6&b=9
    # {"gcd":3}

Test
----

Use Composer to run package PHPSpec and PHPUnit tests:

.. code-block:: bash

    $ cd packages/stdlib; composer test
